package cs151.data_structs.linked

import org.junit.Test

/**
 * TODO: Class description goes here.
 *
 * @author Anthony Benavente
 * @version 4/12/2014
 */
class CDLinkedListTest extends GroovyTestCase {

    @Test
    void testCycle() {

    }

    @Test
    void testSize() {
        CDLinkedList<Integer> list = new CDLinkedList<>();

        // Checking list size is 0 at creation
        assert list.size() == 0;

        // Checking list size is 40 after adding 40 items
        for (int i = 0; i < 40; i++) {
            list.add(i);
        }
        assert list.size() == 40;

        // Checking list size is 39 after removing one item
        list.remove(0);
        assert list.size() == 39;

        // Checking list size keeps decreasing after each item one at a
        // time for 20 items
        list.add(0);
        for (int i = 0; i < 20; i++) {
            list.remove(0);
            assert list.size() == 40 - 1 - i;
        }

        // Checking the list size is 20 after removing 20 items
        assert list.size() == 20;
    }

    @Test
    void testIsEmpty() {
        CDLinkedList<Integer> list = new CDLinkedList<>();
        // Testing list is empty at creation
        assert list.isEmpty();

        // Testing list is not empty after adding 100 items
        for (int i = 0; i < 100; i++) {
            list.add(i);
        }
        assert !list.isEmpty();

        // Testing list is empty after removing added items
        for (int i = 0; i < 100; i++) {
            list.remove(0);
        }
        assert list.isEmpty();
    }

    @Test
    void testContains() {
        CDLinkedList<String> list = new CDLinkedList<>();

        list.add("Anthony");
        list.add("Daniel");
        list.add("Lindsey");
        list.add("Emily");
        list.add("Jairo");
        list.add("Savannah");

        // Checking the list contains all the items that were previously added
        assert list.contains("Anthony");
        assert list.contains("Daniel");
        assert list.contains("Lindsey");
        assert list.contains("Emily");
        assert list.contains("Jairo");
        assert list.contains("Savannah");

        // Checking that the list doesn't contain a name that wasn't added
        assert !list.contains("Frodo");

        // Checking that the list contains a name after adding it
        list.add(0, "Frodo");
        assert list.contains("Frodo");
    }

    @Test
    void testIterator() {

    }

    @Test
    void testToArray() {
        CDLinkedList<Integer> list = new CDLinkedList<>();
        for (int i = 1; i <= 100; i++) {
            list.add(i);
        }

        // Checking that all the objects outputted from toArray have the
        // same values as the items in the list
        Integer[] listArray = (Integer[]) list.toArray();
        for (Integer i : listArray) {
            assert list.contains(i);
        }
    }

    @Test
    void testToArray1() {
        CDLinkedList<Integer> list = new CDLinkedList<>();
        for (int i = 1; i <= 100; i++) {
            list.add(i);
        }

        // Checking that all the objects outputted from toArray have the
        // same values as the items in the list
        Integer[] listArray = new Integer[list.size()];
        list.toArray(listArray);
        for (Integer i : listArray) {
            assert list.contains(i);
        }
    }

    @Test
    void testAdd() {
        CDLinkedList<String> strings = new
                CDLinkedList<String>();

        // Checking that adding items works
        strings.add("Hello,");
        strings.add(" ");
        strings.add("World!");
        assert strings.get(0).equals("Hello,");
        assert strings.get(1).equals(" ");
        assert strings.get(2).equals("World!");

        strings = new CDLinkedList<String>();
        strings.add("1");

        // Checking that the head and tail are the same after adding only
        // one item
        assert strings.get(0).equals("1");
        assert strings.head.data.equals("1");
        assert strings.tail.data.equals("1");
        assert strings.head.next == strings.tail;
        assert strings.tail.prev == strings.head;

        // Checking that the values 1-100 are in the list after adding them
        strings = new CDLinkedList<String>();
        for (int i = 1; i <= 100; i++) {
            strings.add("" + i);
        }

        for (int i = 1; i <= 100; i++) {
            assert strings.get(i-1).equals("" + i);
        }
    }

    @Test
    void testRemove() {

    }

    @Test
    void testContainsAll() {

    }

    @Test
    void testAddAll() {

    }

    @Test
    void testAddAll1() {

    }

    @Test
    void testRemoveAll() {

    }

    @Test
    void testRetainAll() {

    }

    @Test
    void testClear() {

    }

    @Test
    void testGet() {

    }

    @Test
    void testSet() {

    }

    @Test
    void testAddAt() {

    }

    @Test
    void testRemove1() {

    }

    @Test
    void testIndexOf() {

    }

    @Test
    void testLastIndexOf() {

    }

    @Test
    void testListIterator() {

    }

    @Test
    void testListIterator1() {

    }

    @Test
    void testSubList() {

    }
}
