package cs151.data_structs.linked;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * TODO: Class description goes here.
 *
 * @author Anthony Benavente
 * @author Daniel Powell
 * @version 4/9/2014
 */
public class CDLinkedList<T> implements List<T> {

    /** this is the first node in the list */
    private Node<T> head;
    /** this is the last node in the list */
    private Node<T> tail;

    /**
     * The number of elements in the list
     */
    private int size;

    public CDLinkedList() {
        head = new Node<T>(null, null, null);
        tail = new Node<T>(null, null, null);
        head.next = head.prev = tail;
        tail.next = tail.prev = head;
        size = 0;
    }

    public void cycle() {
        // TODO
    }

    /**
     * Returns the number of elements in this list.  If this list contains
     * more than <tt>Integer.MAX_VALUE</tt> elements, returns
     * <tt>Integer.MAX_VALUE</tt>.
     *
     * @return the number of elements in this list
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * Returns <tt>true</tt> if this list contains no elements.
     *
     * @return <tt>true</tt> if this list contains no elements
     */
    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    /**
     * Returns <tt>true</tt> if this list contains the specified element.
     * More formally, returns <tt>true</tt> if and only if this list contains
     * at least one element <tt>e</tt> such that
     * <tt>(o==null&nbsp;?&nbsp;e==null&nbsp;:&nbsp;o.equals(e))</tt>.
     *
     * @param o element whose presence in this list is to be tested
     * @return <tt>true</tt> if this list contains the specified element
     */
    @Override
    public boolean contains(Object o) {
        boolean result = false;
        Node<T> walker = head;
        for (int i = 0; !result && i < size; i++, walker = walker.next) {
            result = o.equals(walker.data);
        }
        return result;
    }

    /**
     * Returns an iterator over the elements in this list in proper sequence.
     *
     * @return an iterator over the elements in this list in proper sequence
     */
    @Override
    public Iterator<T> iterator() {
        return new CircleyDoubleyLinkedListIterator();
    }

    /**
     * Returns an array containing all of the elements in this list in proper
     * sequence (from first to last element).
     *
     * @return an array containing all of the elements in this list in proper
     * sequence
     */
    @Override
    public Object[] toArray() {
        Object[] objects = new Object[size];
        Node<T> walker = head;
        for (int i = 0; i < size; i++, walker = walker.next) {
            objects[i] = walker.data;
        }
        return objects;
    }

    /**
     * Returns an array containing all of the elements in this list in
     * proper sequence (from first to last element)
     *
     * @param a the array into which the elements of this list are to
     *          be stored, if it is big enough; otherwise, a new array of the
     *          same runtime type is allocated for this purpose.
     * @return an array containing the elements of this list
     */
    @Override
    public <T1> T1[] toArray(T1[] a) {
        if (a.length < size) a = (T1[]) new Object[size];
        Node<T> walker = head;
        for (int i = 0; i < size; i++, walker = walker.next) {
            a[i] = (T1) walker.data;
        }
        return a;
    }

    /**
     * Appends the specified element to the end of this list (optional
     * operation).
     * <p/>
     * <p>Lists that support this operation may place limitations on what
     * elements may be added to this list.  In particular, some
     * lists will refuse to add null elements, and others will impose
     * restrictions on the type of elements that may be added.  List
     * classes should clearly specify in their documentation any restrictions
     * on what elements may be added.
     *
     * @param t element to be appended to this list
     * @return <tt>true</tt> (as specified by {@link Collection#add})
     */
    @Override
    public boolean add(T t) {
        Node<T> toAdd = new Node<T>(t, null, null);

        if (size == 0) {
            toAdd.next = head;
            toAdd.prev = tail;
            head = toAdd;
            tail = toAdd;
            head.next = toAdd;
            tail.next = toAdd;
            head.prev = toAdd;
            tail.prev = toAdd;
        } else {
            toAdd.next = tail.next;
            toAdd.prev = tail;
            tail.next = toAdd;
            tail = tail.next;
        }

        size++;

        return true;
    }

    /**
     * Removes the first occurrence of the specified element from this list,
     * if it is present (optional operation).  If this list does not contain
     * the element, it is unchanged.  More formally, removes the element with
     * the lowest index <tt>i</tt> such that
     * <tt>(o==null&nbsp;?&nbsp;get(i)==null&nbsp;:&nbsp;o.equals(get(i)))</tt>
     * (if such an element exists).  Returns <tt>true</tt> if this list
     * contained the specified element (or equivalently, if this list changed
     * as a result of the call).
     *
     * @param o element to be removed from this list, if present
     * @return <tt>true</tt> if this list contained the specified element
     */
    @Override
    public boolean remove(Object o) {
        Node<T> walker = head;
        boolean result = false;

        while (!result && walker != null) {
            if (o.equals(walker.data)) {
                walker.prev.next = walker.next;
                walker.next.prev = walker.prev;
                result = true;
            }
            walker = walker.next;
        }

        size--;

        return result;
    }

    /**
     * Returns <tt>true</tt> if this list contains all of the elements of the
     * specified collection.
     *
     * @param c collection to be checked for containment in this list
     * @return <tt>true</tt> if this list contains all of the elements of the
     * specified collection
     */
    @Override
    public boolean containsAll(Collection<?> c) {
        boolean result = true;
        for (Object object : c) {
            result &= contains(object);
            if (!result) break;
        }
        return result;
    }

    /**
     * Inserts all of the elements in the specified collection into this
     * list at the specified position (optional operation).  Shifts the
     * element currently at that position (if any) and any subsequent
     * elements to the right (increases their indices).  The new elements
     * will appear in this list in the order that they are returned by the
     * specified collection's iterator.  The behavior of this operation is
     * undefined if the specified collection is modified while the
     * operation is in progress.  (Note that this will occur if the specified
     * collection is this list, and it's nonempty.)
     *
     * @param index index at which to insert the first element from the
     *              specified collection
     * @param c     collection containing elements to be added to this list
     * @return <tt>true</tt> if this list changed as a result of the call
     */
    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        Node<T> toAddAfter = getNode(index);
        Node<T> oldNext = toAddAfter.next;
        Node<T> walker = toAddAfter;
        for (Object o : c) {
            Node<T> toAdd = new Node<T>((T) o);
            toAdd.prev = walker;
            walker.next = toAdd;
            walker = walker.next;
        }
        walker.next = oldNext;
        oldNext.prev = walker.next;

        return true;
    }

    /**
     * Appends all of the elements in the specified collection to the end of
     * this list, in the order that they are returned by the specified
     * collection's iterator (optional operation).  The behavior of this
     * operation is undefined if the specified collection is modified while
     * the operation is in progress.  (Note that this will occur if the
     * specified collection is this list, and it's nonempty.)
     *
     * @param c collection containing elements to be added to this list
     * @return <tt>true</tt> if this list changed as a result of the call
     */
    @Override
    public boolean addAll(Collection<? extends T> c) {
        return addAll(size - 1, c);
    }

    /**
     * Removes from this list all of its elements that are contained in the
     * specified collection (optional operation).
     *
     * @param c collection containing elements to be removed from this list
     * @return <tt>true</tt> if this list changed as a result of the call
     */
    @Override
    public boolean removeAll(Collection<?> c) {
        // TODO
        return false;
    }

    /**
     * Retains only the elements in this list that are contained in the
     * specified collection (optional operation).  In other words, removes
     * from this list all of its elements that are not contained in the
     * specified collection.
     *
     * @param c collection containing elements to be retained in this list
     * @return <tt>true</tt> if this list changed as a result of the call
     */
    @Override
    public boolean retainAll(Collection<?> c) {
        // TODO
        return false;
    }

    /**
     * Removes all of the elements from this list (optional operation).
     * The list will be empty after this call returns.
     *
     * @throws UnsupportedOperationException if the <tt>clear</tt> operation
     *                                       is not supported by this list
     */
    @Override
    public void clear() {
        head = null;
        tail = null;
        size = 0;
    }

    /**
     * Returns the element at the specified position in this list.
     *
     * @param index index of the element to return
     * @return the element at the specified position in this list
     * @throws IndexOutOfBoundsException if the index is out of range
     *                                   (<tt>index &lt; 0 || index &gt;= size()</tt>)
     */
    @Override
    public T get(int index) {
        Node<T> result = getNode(index);
        return result.data;
    }

    /**
     * Replaces the element at the specified position in this list with the
     * specified element (optional operation).
     *
     * @param index   index of the element to replace
     * @param element element to be stored at the specified position
     * @return the element previously at the specified position
     */
    @Override
    public T set(int index, T element) {
        Node<T> toChange = getNode(index);
        T data = toChange.data;
        toChange.data = element;
        return data;
    }

    /**
     * Adds an element to the list at the given index
     *
     * @param index   index at which to insert the new element
     * @param element the element to add
     */
    @Override
    public void add(int index, T element) {
        Node<T> toAddAfter = getNode(index);
        Node<T> toAdd = new Node<T>(element, toAddAfter, toAddAfter.next);
        toAddAfter.next.prev = toAdd;
        toAddAfter.next = toAdd;
        size++;
    }

    /**
     * Removes the element at the specified position in this list (optional
     * operation).  Shifts any subsequent elements to the left (subtracts one
     * from their indices).  Returns the element that was removed from the
     * list.
     *
     * @param index the index of the element to be removed
     * @return the element previously at the specified position
     */
    @Override
    public T remove(int index) {
        Node<T> toRemove = getNode(index);
        T data = toRemove.data;
        if (size > 1) {
            toRemove.prev.next = toRemove.next;
            toRemove.next.prev = toRemove.prev;
        } else {
            head.data = tail.data = null;
            head.next = head.prev = tail;
            tail.next = tail.prev = head;
        }
        size--;
        return data;
    }

    /**
     * Returns the index of the first occurrence of the specified element
     * in this list, or -1 if this list does not contain the element.
     *
     * @param o element to search for
     * @return the index of the first occurrence of the specified element in
     * this list, or -1 if this list does not contain the element
     */
    @Override
    public int indexOf(Object o) {
        int index = -1;
        int counter = 0;
        boolean found = false;
        Node<T> walker = head;
        for (; !found && walker.next != head; walker = walker.next,
                counter++) {
            if (o.equals(walker.data)) {
                index = counter;
                found = true;
            }
        }
        return index;
    }

    /**
     * Gets the last index at which the specified object appears in this list
     *
     * @param o the object to look for
     * @return the index indicating the last position that the object is at
     */
    @Override
    public int lastIndexOf(Object o) {
        int index = -1;
        int counter = size - 1;
        boolean found = false;
        Node<T> walker = tail;
        for (; !found && walker.prev != tail; walker = walker.prev,
                counter--) {
            if (o.equals(walker.data)) {
                found = true;
                index = counter;
            }
        }
        return index;
    }


    /**
     * Returns a list iterator over the elements in this list (in proper
     * sequence).
     *
     * @return a list iterator over the elements in this list (in proper
     * sequence)
     */
    @Override
    public ListIterator<T> listIterator() {
        throw new RuntimeException("Operation is not supported...");
    }

    /**
     * Returns a list iterator over the elements in this list (in proper
     * sequence), starting at the specified position in the list.
     *
     * @param index index of the first element to be returned from the
     *              list iterator (by a call to
     *              {@link java.util.ListIterator#next next})
     * @return a list iterator over the elements in this list (in proper
     * sequence), starting at the specified position in the list
     */
    @Override
    public ListIterator<T> listIterator(int index) {
        throw new RuntimeException("Operation is not supported...");
    }

    /**
     * Returns a view of the portion of this list between the specified
     * <tt>fromIndex</tt>, inclusive, and <tt>toIndex</tt>, exclusive.
     *
     * @param fromIndex low endpoint (inclusive) of the subList
     * @param toIndex   high endpoint (exclusive) of the subList
     * @return a view of the specified range within this list
     */
    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        List<T> subList = new CDLinkedList<T>();
        Node<T> walker = getNode(fromIndex);
        for (int i = fromIndex; i < toIndex; i++, walker = walker.next) {
            subList.add(walker.data);
        }
        return subList;
    }

    /**
     * Gets the node at a given index
     *
     * @param index The index of the node to get
     * @return the node at the specified index
     * @throws java.lang.IndexOutOfBoundsException if the given index is
     * not in bounds of the list
     */
    private Node<T> getNode(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("The index is out of bounds");
        }

        Node<T> walker = head;
        for (int i = 0; i < index; i++, walker = walker.next);
        return walker;
    }

/**####################Inner Class#########################################*/
    private class Node<T> {

        /** This is a reference to the Node before this node */
        private Node prev;

        /** This is a reference to the Node after this node */
        private Node next;

        /** This is the data that this node contains */
        private T data;

        /**
         * This constructor creates a node with all default values of null
         */
        public Node() {
            this(null, null, null);
        }

        public Node(T data) {
            this(data, null, null);
        }

        /** This is the constructor for this node that allows its position and
         * data to be set
         * @param data the data in the node
         * @param next the node after this one
         * @param prev the node before this one
         */
        private Node(T data, Node next, Node prev) {
            this.data = data;
            this.next = next;
            this.prev = prev;
        }
    }
/** ********************Inner Class End*********************************** */

    // TODO: Finish the iterator, stops when iterator reaches tail/size
/**####################Inner Class#########################################*/
    private class CircleyDoubleyLinkedListIterator implements
            ListIterator<T> {

        /**
         * Returns {@code true} if the iteration has more elements.
         * (In other words, returns {@code true} if {@link #next} would
         * return an element rather than throwing an exception.)
         *
         * @return {@code true} if the iteration has more elements
         */
        @Override
        public boolean hasNext() {
            return false;
        }

        /**
         * Returns the next element in the iteration.
         *
         * @return the next element in the iteration
         */
        @Override
        public T next() {
            return null;
        }

        /**
         * Returns {@code true} if this list iterator has more elements when
         * traversing the list in the reverse direction.  (In other words,
         * returns {@code true} if {@link #previous} would return an element
         * rather than throwing an exception.)
         *
         * @return {@code true} if the list iterator has more elements when
         * traversing the list in the reverse direction
         */
        @Override
        public boolean hasPrevious() {
            return false;
        }

        /**
         * Returns the previous element in the list and moves the cursor
         * position backwards.  This method may be called repeatedly to
         * iterate through the list backwards, or intermixed with calls to
         * {@link #next} to go back and forth.  (Note that alternating calls
         * to {@code next} and {@code previous} will return the same
         * element repeatedly.)
         *
         * @return the previous element in the list
         */
        @Override
        public T previous() {
            return null;
        }

        /**
         * Returns the index of the element that would be returned by a
         * subsequent call to {@link #next}. (Returns list size if the list
         * iterator is at the end of the list.)
         *
         * @return the index of the element that would be returned by a
         * subsequent call to {@code next}, or list size if the list
         * iterator is at the end of the list
         */
        @Override
        public int nextIndex() {
            return 0;
        }

        /**
         * Returns the index of the element that would be returned by a
         * subsequent call to {@link #previous}. (Returns -1 if the list
         * iterator is at the beginning of the list.)
         *
         * @return the index of the element that would be returned by a
         * subsequent call to {@code previous}, or -1 if the list
         * iterator is at the beginning of the list
         */
        @Override
        public int previousIndex() {
            return 0;
        }

        /**
         * The circley doubley linked list iterator does not support removal
         */
        @Override
        public void remove() {
            throw new RuntimeException("Remove is not supported");
        }

        /**
         * Replaces the last element returned by {@link #next} or
         * {@link #previous} with the specified element (optional operation).
         * This call can be made only if neither {@link #remove} nor {@link
         * #add} have been called after the last call to {@code next} or
         * {@code previous}.
         *
         * @param t the element with which to replace the last element returned by
         *          {@code next} or {@code previous}
         * @throws UnsupportedOperationException if the {@code set} operation
         *                                       is not supported by this list iterator
         * @throws ClassCastException            if the class of the specified element
         *                                       prevents it from being added to this list
         * @throws IllegalArgumentException      if some aspect of the specified
         *                                       element prevents it from being added to this list
         * @throws IllegalStateException         if neither {@code next} nor
         *                                       {@code previous} have been called, or {@code remove} or
         *                                       {@code add} have been called after the last call to
         *                                       {@code next} or {@code previous}
         */
        @Override
        public void set(T t) {

        }

        /**
         * Inserts the specified element into the list (optional operation).
         * The element is inserted immediately before the element that
         * would be returned by {@link #next}, if any, and after the element
         * that would be returned by {@link #previous}, if any.  (If the
         * list contains no elements, the new element becomes the sole element
         * on the list.)  The new element is inserted before the implicit
         * cursor: a subsequent call to {@code next} would be unaffected, and a
         * subsequent call to {@code previous} would return the new element.
         * (This call increases by one the value that would be returned by a
         * call to {@code nextIndex} or {@code previousIndex}.)
         *
         * @param t the element to insert
         * @throws UnsupportedOperationException if the {@code add} method is
         *                                       not supported by this list iterator
         * @throws ClassCastException            if the class of the specified element
         *                                       prevents it from being added to this list
         * @throws IllegalArgumentException      if some aspect of this element
         *                                       prevents it from being added to this list
         */
        @Override
        public void add(T t) {

        }
    }
/** ********************Inner Class End*********************************** */
}


